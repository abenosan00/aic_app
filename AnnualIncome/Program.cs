﻿using System;
using System.Collections.Generic;

namespace AnnualIncome
{   
    /// <summary>
    /// 実行クラス
    /// </summary>
    class Program
    {
        /// <summary>
        /// 実行メソッド
        /// </summary>
        /// <param name="args">コマンドライン引数</param>
        static void Main(string[] args)
        {

            Console.Write("あなたの名前を入力してください>>>");

            // 入力されたユーザ名を取得
            var entryUserName = Console.ReadLine();
            var valCheckFlg = false;
            var entryAnnualIncome = 0;

            // 入力値のバリデーションチェック用ループ
            while (!valCheckFlg) 
            {
                Console.WriteLine("次に{0}さんの年収を入力してください:", entryUserName);
                Console.WriteLine("【例:300万の場合 => 300 Enter】");

                // 入力された年収[万円]
                if (int.TryParse(Console.ReadLine(), out int input))
                {
                    entryAnnualIncome = input;
                    valCheckFlg = true;
                }
                else
                {
                    Console.WriteLine("半角で入力してください。");
                }
            }

            // 年収から出力する用の給料演算結果を取得
            var resultSalarys = CalculateSalary(entryAnnualIncome);

            Console.WriteLine("{0}さんの年月日時分秒の給料は・・・", entryUserName);

            // 出力
            foreach (var resultSalary in resultSalarys) {
                Console.WriteLine(string.Format("{0}:{1}円", resultSalary.Key, resultSalary.Value.ToString("#,0")));
            }

            // 年収アドバイスを出力
            outputSalaryComment(entryAnnualIncome, entryUserName);

        }


        // 計算メソッド
        /// <summary>
        /// 年収から月給、日給、時給、分給、秒給を計算して返す
        /// </summary>
        /// <returns>年収、月給などの計算結果</returns>
        /// <param name="entryAnnualIncome">年収[万円]</param>
        static Dictionary<string, int> CalculateSalary(int entryAnnualIncome)
        {
            // 年収[万円]を円単位に変換
            var initAnnualIncome = entryAnnualIncome * 10000;
            // 1年間の月数
            var months = 12;
            // 1カ月の一般的な業務日数 土日休みを考慮
            var days = 20;
            // 1日の一般的な操業時間
            var hours = 8;

            // 戻り値用のオブジェクト生成
            var resultCalculateSalarys = new Dictionary<string, int>();

            resultCalculateSalarys.Add("月給", initAnnualIncome / months);
            resultCalculateSalarys.Add("日給", initAnnualIncome / months / days);
            resultCalculateSalarys.Add("時給", initAnnualIncome / months / days / hours);
            resultCalculateSalarys.Add("分給", initAnnualIncome / months / days / hours / 60);
            resultCalculateSalarys.Add("秒給", initAnnualIncome / months / days / hours / 60 / 60);

            return resultCalculateSalarys;
        }

        /// 年収コメント出力メソッド
        /// <summary>
        /// 年収を分析し、アドバイスを返す。
        /// </summary>
        /// <param name="entryAnnualIncome">年収[万円]</param>
        static void outputSalaryComment(int entryAnnualIncome, string entryUserName)
        {
            // 会長・社長クラス　一般平均年収3693万円
            var chairmanClass = 3693;
            // 部長クラス　一般平均年収800万円
            var departmentChiefClass = 800;
            // 従業員クラス　一般平均年収424万円
            var employeeClass = 424;
            // アルバイトクラス 一般平均年収103万円
            var albiteClass = 103;

            if (entryAnnualIncome > chairmanClass)
            {
                Console.WriteLine("きっと{0}さん、あなたは世界が認める大スターでしょう！脱税だけはしないように！", entryUserName);
            }
            else if (entryAnnualIncome > departmentChiefClass)
            {
                Console.WriteLine("{0}さん、あなたはエリート級の人材です！", entryUserName);
            }
            else if (entryAnnualIncome > employeeClass)
            {
                Console.WriteLine("{0}さん、あなたは普通な人です！", entryUserName);
            }
            else if (entryAnnualIncome > albiteClass)
            {
                Console.WriteLine("{0}さん、あなたはアルバイトレベルの収入ですね・・・笑！", entryUserName);
            }
            else 
            {
                Console.WriteLine("ん〜生きる価値なし。！");
            }
        }
    }
}
